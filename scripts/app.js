/**
*	CryptPast
*	A secure and open-source crypted pasting service.
*	Made by ThanosS.
*
*	CryptPast is under MIT license
**/

function htmlentities(str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

// Define our object with functions.
var CryptPast = {
	error: null,
	success: null,
	warn: null,
	form: null,
	state: "crypt",

	init: function()
	{
		console.log("Feel free to inspect the source code ! :)");

		this.error = document.getElementById("error");
		this.success = document.getElementById("success");
		this.warn = document.getElementById("warn");
		this.form = document.getElementById("cryptpast");
		this.view = document.querySelector(".view");
		this.textarea = document.querySelector(".text");
		
		sjcl.random.startCollectors();
		return this;
	},

	generatePass: function()
	{
		// Check that enough entropy has been collected
		if (sjcl.random.isReady())
		{
			try
			{
				// Populate the password with 12 random words encoded as a Base64 string equivalent to 64 characters
				this.form.password.value = sjcl.codec.base64.fromBits(sjcl.random.randomWords(12));
				this.form.password.select();
			}
			catch(e)
			{
				this.error.innerHTML = e;
				this.error.style.display = "block";
			}
		}
		else
		{
			this.error.innerHTML = "Random number generator requires more entropy";
			this.error.style.display = "block";
		}
	},

	encrypt: function()
	{
		this.error.innerHTML = '<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.error.style.display=\'none\';">×</button>';
		this.success.innerHTML = '<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.success.style.display=\'none\';">×</button>';
		this.warn.innerHTML = '<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.warn.style.display=\'none\';">×</button>';
		this.error.style.display = "none";
		this.success.style.display = "none";
		this.warn.style.display = "none";

		// Validate the plain text, password and expiration fields
		if (this.form.cipher.value.length > 0 && this.form.password.value.length > 0 && this.form.expiration.selectedIndex > 0)
		{
			if (this.form.password.value.length < 8)
			{
				this.warn.innerHTML += "Password is too short (" + this.form.password.value.length + " characters)";
				this.warn.style.display = "block";
			}

			// Keep a temporary copy of the plain text to restore if unsuccessful
			var original = this.form.cipher.value;

			try
			{
				// Encrypt the plain text using the password to a Base64 string
				this.form.cipher.value = sjcl.codec.base64.fromBits(sjcl.codec.utf8String.toBits(sjcl.encrypt(this.form.password.value, this.form.cipher.value, {ks: 256})));

				// Check that the cipher text is below the maximum character limit
				if (this.form.cipher.value.length < 16777215)
				{
					// Clear the password and plain text as a security measure
					this.form.password.value = null;
					original = null;
					
					var response   = null,
					messageDiv = this.success;

					$.post("paste.php", {
						action: 'new',
						paste: this.form.cipher.value,
						expire: this.form.expiration.selectedIndex
					})
					.done(
						function(resp)
						{
							response = resp;
						}
						)
					.always(
						function()
						{
							response = JSON.parse(response);
							messageDiv.innerHTML += "<b>Succes</b> ! Your CryptPast is available here: <a href=\"https://hapshack.eu/cryptpast/#!paste/" + response.id + "\">https://hapshack.eu/cryptpast/#!paste/" + response.id + "</a>"; 
							messageDiv.style.display = "block";
							//$('textarea[name=cipher]').autoHeight();
							this.form.cipher.style.height = "auto";
						}
						);
				}
				else
				{
					// Restore the original plain text from the temporary copy
					this.form.cipher.value = original;
					original = null;

					this.error.innerHTML += "Maximum length exceeded";
					this.error.style.display = "block";
				}
			}
			catch(e)
			{
				this.error.innerHTML += e;
				this.error.style.display = "block";
			}
		}
		else
		{
			this.error.innerHTML += "One or more required fields were left blank";
			this.error.style.display = "block";
		}
	},

	decrypt: function(type)
	{
		this.error.innerHTML = '<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.error.style.display=\'none\';">×</button>';
		this.success.innerHTML = '<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.success.style.display=\'none\';">×</button>';
		this.warn.innerHTML = '<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.warn.style.display=\'none\';">×</button>';
		this.error.style.display = "none";
		this.success.style.display = "none";
		this.warn.style.display = "none";

		// Validate the cipher text and password fields
		if (this.form.cipher.value.length > 0 && this.form.password.value.length > 0)
		{
			try
			{
				// Decrypt the cipher text using the password
				if (type == 1)
				{
					// Use SJCL from a Base64 string
					var result = sjcl.decrypt(this.form.password.value, sjcl.codec.utf8String.fromBits(sjcl.codec.base64.toBits(this.form.cipher.value)), {ks: 256});
					this.view.innerHTML = "<code>" + htmlentities(result);
					this.view.innerHTML += "</code>";
				}
				else
				{
					// Use Movable Type for backwards compatibility with previous version
					var result = Aes.Ctr.decrypt(this.form.cipher.value, this.form.password.value, 256);
					this.view.innerHTML = "<code>" + htmlentities(result);
					this.view.innerHTML += "</code>";
				}

				// Clear the password as a security measure
				this.form.password.value = null;
				//$(this.form.cipher.id).addClass("disabled");
				//$('textarea[name=cipher]').autoHeight();

				this.view.style.display = "block";
				this.textarea.style.display = "none";
				$('pre code').each(function(i, block)
				{
					hljs.configure({ tabReplace: '    ' });
					hljs.highlightBlock(block);
				});
				var textareaValue = this.view.innerHTML;
				textareaValue.replace('	','  ');
				
			}
			catch(e)
			{
				this.error.innerHTML += e;
				this.error.style.display = "block";
			}
		}
		else
		{
			this.error.innerHTML += "A required field was left blank";
			this.error.style.display = "block";
		}
	}
};

jQuery.fn.extend({
	autoHeight: function () {
		function autoHeight_(element) {
			return jQuery(element)
			.css({ 'height': 'auto', 'overflow-y': 'hidden' })
			.height(element.scrollHeight);
		}
		return this.each(function() {
			autoHeight_(this).on('input', function() {
				autoHeight_(this);
			});
		});
	}
});

$(document).delegate('textarea', 'keydown', function(e) {
	var keyCode = e.keyCode || e.which;

	if (keyCode == 9) {
		e.preventDefault();
		var start = $(this).get(0).selectionStart;
		var end = $(this).get(0).selectionEnd;

    // set textarea value to: text before caret + tab + text after caret
    $(this).val($(this).val().substring(0, start)
    	+ "\t"
    	+ $(this).val().substring(end));

    // put caret at right position again
    $(this).get(0).selectionStart =
    $(this).get(0).selectionEnd = start + 1;
}
});

document.addEventListener("DOMContentLoaded",
	function(event)
	{
		document.CryptPast = CryptPast.init();
		pasteSystem();
		//$('textarea[name=cipher]').autoHeight();

		$('pre code').each(function(i, block)
		{
			hljs.highlightBlock(block);
		});

		$(document.CryptPast.form.id).on("submit",
			function(event)
			{
				event.preventDefault();

				if (document.CryptPast.state == "crypt")
					$(document.CryptPast.form.crypt.id).click();
				else if (document.CryptPast.state == "decrypt")
					$(document.CryptPast.form.decrypt.id).click();
			}
			);
	}
	);

var pasteSystem = function()
{
	var hash = window.location.hash.replace(/^#!/,'');
	var args = hash.split('/');

	if (args[0] == "new" || hash == "")
	{
		$(".tagline").text("Create a new CryptPast");
		document.CryptPast.view.style.display = "none";
		document.CryptPast.textarea.style.display = "block";
		document.CryptPast.state = "crypt";
		document.CryptPast.form.style.display = "block";
		document.CryptPast.form.cipher.value = "";
		document.CryptPast.form.cipher.style.height = "auto";
		document.CryptPast.form.expiration.style.display = "block";
		document.CryptPast.form.generate.style.display = "block";
		document.CryptPast.form.reset.style.display = "inline-block";
		document.CryptPast.form.crypt.style.display = "inline-block";
		document.CryptPast.form.decrypt.style.display = "none";

		$('pre code').each(function(i, block)
		{
			hljs.highlightBlock(block);
		});

		return;
	}

	if (args[0] == "about" || hash == "")
	{
		$(".tagline").html('Coming soon...');
		document.CryptPast.form.style.display = "none";
		return;
	}

	if (args[0] == "contact" || hash == "")
	{
		$(".tagline").html('Coming soon...');
		document.CryptPast.form.style.display = "none";
		return;
	}

	if (args[0] == "paste" || hash == "")
	{
		var response = null;
		$.post("paste.php", {
			action: 'get',
			id: args[1],
		})
		.done(
			function(resp)
			{
				response = resp;
			}
			)
		.always(
			function()
			{
				response = JSON.parse(response);

				if (response.status == 404)
				{
					document.CryptPast.error.innerHTML += "The requested paste has expired.";
					document.CryptPast.error.style.display = "block";
					document.querySelector(".tagline").innerHTML = 'The CryptPast you requested has expired or may have never been here.';
					document.CryptPast.form.style.display = "none";
					return;
				}


				$(".tagline").text("Enter the password for this CryptPast");
				document.CryptPast.state = "decrypt";
				document.CryptPast.form.style.display = "block";
				document.CryptPast.form.cipher.value = response.paste;
				document.CryptPast.form.expiration.style.display = "none";
				document.CryptPast.form.generate.style.display = "none";
				document.CryptPast.form.reset.style.display = "none";
				document.CryptPast.form.crypt.style.display = "none";
				document.CryptPast.form.decrypt.style.display = "block";
				
				$('pre code').each(function(i, block)
				{
					hljs.highlightBlock(block);
				});

			}
			);
}
}

$(window).bind('hashchange', pasteSystem);
