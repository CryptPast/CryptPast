<?php
$_CONFIG = [
	'mysql_host'   => 'localhost',
	'mysql_dbname' => 'cryptpast',
	'mysql_user'   => 'root',
	'mysql_pass'   => '',
	'salt'         => sha1('my-simple-salt')
];