<?php
/**
*	CryptPast
*	A secure and open-source crypted pasting service.
*	Made by ThanosS.
*
*	CryptPast is under MIT license
**/

class CryptPastConfig
{
	private $_CONFIG = null;

	public function __construct()
	{
		require_once('configFile.php');
		$this->_CONFIG = $_CONFIG;
	}

	public function get($key)
	{
		if (array_key_exists($key, $this->_CONFIG))
			return $this->_CONFIG[$key];
		else
			throw new Exception("ConfigManager: Key don't exists.", 1);	
	}
}

class CryptPast
{
	private $config = null;
	private $db = null;

	public function __construct()
	{
		$this->config  = new CryptPastConfig();

		$dbh = 'mysql:host=' . $this->config->get('mysql_host') . ';dbname=' . $this->config->get('mysql_dbname');
		$this->db = new PDO($dbh, $this->config->get('mysql_user'), $this->config->get('mysql_pass'));
		$this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	}

	private function processTime($timestamp, $value)
	{
		$date = DateTime::createFromFormat('U', $timestamp, new DateTimeZone('GMT'));

		switch ($value) {
			case 1:
			$date->add(new DateInterval('PT10M'));
			break;
			
			case 2:
			$date->add(new DateInterval('PT1H'));
			break;

			case 3:
			$date->add(new DateInterval('P1D'));
			break;

			case 4:
			$date->add(new DateInterval('P7D'));
			break;

			case 5:
			$date->add(new DateInterval('P1M'));
			break;

			case 6:
			$date->add(new DateInterval('P1Y'));
			break;

			case 7:
			$date->add(new DateInterval('P10Y'));
			break;
		}

		return $date->format('Y-m-d H:i:s');
	}

	public function newPaste($paste, $expire)
	{
		$id = substr(sha1($this->config->get('salt') . $paste), 0, 8); // Generate a unique 8 char identifier.
		$expiration = $this->processTime(time(), $expire);

		$request = $this->db->prepare('INSERT INTO pastes(id, hash, expire, date) VALUES(:id, :hash, :expire, CURRENT_TIMESTAMP)');
		$request->execute(array(
			':id' => $id,
			':hash' => $paste,
			':expire' => $expiration
			));

		return json_encode([
			"id" => $id
			]);
	}

	public function getPaste($id)
	{
		$request = $this->db->prepare('SELECT id, hash, expire FROM pastes WHERE id = :id');
		$request->execute(array(
			':id' => $id
			));

		foreach($request->fetchAll() as $row)
		{
			if (strtotime($row->expire) <= time())
			{
				$deleteReq = $this->db->prepare('DELETE FROM pastes WHERE id = :id');
				$deleteReq->execute(array(
					':id' => $id
					));
				
				return json_encode(array(
					"id" => $row->id,
					"status" => 404
					));
			}
			
			return json_encode(array(
				"id" => $row->id,
				"status" => 200,
				"paste" => $row->hash,
				));
		}

		return json_encode(array(
			"id" => $id,
			"status" => 404
			));
	}
}
