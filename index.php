<!DOCTYPE html>
<html>
<head>
	<title>CryptPast - Crypted Pasting service !</title>
	<meta charset="utf-8">
	<link type="text/css" rel="stylesheet" href="styles/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="styles/design.css<?php echo "?".rand(); ?>">

	<script type="text/javascript" src="scripts/sjcl.js">/* /* Stanford Javascript Crypto Library */ */</script>
	<script type="text/javascript" src="scripts/aes.js">/* AES (Advanced Encryption Standard) implementation from Movable Type for backwards compatibility */</script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">/* jQuery */</script>
	<script type="text/javascript" src="scripts/app.js<?php echo "?".rand(); ?>">/* CryptPast Client-Side Script */</script>
	<link rel="stylesheet" href="https://rawgit.com/isagalaev/highlight.js/master/src/styles/androidstudio.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.5/highlight.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<nav>
				<ul class="nav nav-pills pull-right">
					<li role="presentation" class="active"><a href="#!new">New CryptPast</a></li>
					<li role="presentation"><a href="#!about">About</a></li>
					<li role="presentation"><a href="#!contact">Contact</a></li>
				</ul>
			</nav>
			<h3 class="text-muted">CryptPast</h3>
		</div>
		
		<div id="noscript" class="alert alert-dismissible alert-danger">
			<b>Javascript must be enabled</b> in your browser in order to use CryptPast. Please <b>enable</b> it or you will not be able to use this website.
		</div>

		<div id="error" class="alert alert-dismissible alert-danger">
			<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.error.style.display='none';">×</button>
			<p id="msg"></p>
		</div>
		<div id="warn" class="alert alert-dismissible alert-warning">
			<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.warn.style.display='none';">×</button>
			<p id="msg"></p>
		</div>
		<div id="success" class="alert alert-dismissible alert-success">
			<button type="button" class="close" data-dismiss="alert" onclick="document.CryptPast.success.style.display='none';">×</button>
			<p id="msg"></p>
		</div>
		<div class="jumbotron">
			<h3 class="tagline">Create a new CryptPast</h3>

			<form id="cryptpast" method="post" onsubmit="return false;" autocomplete="off">
				<div class="form-group">
					<div class="col-lg-12">
						<textarea class="form-control text" name="cipher" rows="10"></textarea>
						<pre class="view"><code class="php"></code></pre>
						<span class="help-block">Enter the text you want to CryptPast here !</span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-lg-2">
						<select class="form-control" name="expiration">
							<option value="" disabled selected>Expiration Time</option>
							<option value="10minutes">10 Minutes</option>
							<option value="hour">1 Hour</option>
							<option value="day">1 Day</option>
							<option value="week">1 Week</option>
							<option value="month">1 Month</option>
							<option value="year">1 Year</option>
							<option value="infinite">Never expire</option>
						</select>
					</div>
					<div class="col-lg-7">
						<div class="input-group">
							<label class="input-group-addon" for="password">Password</label>
							<input class="form-control" type="text" name="password">
							<span class="input-group-btn">
								<button class="btn btn-default" name="generate" type="button" onclick="document.CryptPast.generatePass();">Generate</button>
							</span>
						</div>
						<span class="help-block">Do not forget to copy the password or you will never be able to decrypt</span>
					</div>
					<div class="col-lg-3">
						<input type="reset" class="btn btn-default" name="reset" value="Reset">
						<input type="button" class="btn btn-success" name="crypt" value="CryptPast it" onclick="document.CryptPast.encrypt();">
						<input type="button" class="btn btn-success" name="decrypt" value="Decrypt" onclick="document.CryptPast.decrypt(1);">
					</div>
				</div>

				<p>&nbsp;</p>				
			</form>
		</div>

		<footer class="footer">
			<p>
				<span class="copyleft">&copy;</span> Copyleft CryptPast - Developped by ThanosS
				<span class="pull-right">
					<a href="bitcoin:1Q82X4ykYaLtLHRmGgbJUZhN2cHknFBbRL">BTC: 1Q82X4ykYaLtLHRmGgbJUZhN2cHknFBbRL</a> - 
					<a href="https://github.com/CryptPast/CryptPast" target="_blank">View the code on GitHub</a>
				</span>
			</p>
		</footer>

	</div> <!-- /container -->
	<script>document.querySelector('#noscript').style.display = 'none';</script>
</body>
</html>