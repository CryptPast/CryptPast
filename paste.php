<?php
/**
*	CryptPast
*	A secure and open-source crypted pasting service.
*	Made by ThanosS.
*
*	CryptPast is under MIT license
**/

//if (!isset($_POST['action'])) die('DIRECT SCRIPT ACCESSING IS FORBIDEN.');

require_once('class/CryptPast.class.php');

$action = $_POST['action'];

$cryptPast = new CryptPast();

switch ($action)
{
	case 'new':
		$paste  = $_POST['paste'];
		$expire = $_POST['expire'];

		//header('Content-Type: text/json');
		die($cryptPast->newPaste($paste, $expire));
		break;

	case 'get':
		$id = $_POST['id'];

		//header('Content-Type: text/json');
		die($cryptPast->getPaste($id));
		break;
}