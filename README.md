# CryptPast - Crypted Pasting Service

CryptPast is the new way to make paste and share them ! All is browser-side crypted to provide a more better privacy.  
  
The only data stored are : id, crypted text, date of creation, date of removing... And that's all ! No password are stored into the database. The decryption is only possible if user owns the password/key.

# Screenshot
![Preview of general UI](http://i.imgur.com/wzVuwKw.png)
![Preview of syntax highlighting](http://i.imgur.com/wAJTbTS.png)
